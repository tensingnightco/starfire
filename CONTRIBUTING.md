# Forgejo Contributor Guide

The Forgejo project is run by a community of people who are expected to follow this guide when cooperating on a simple bug fix as well as when changing the governance. For more information about the project, take a look at [the documentation explaining what Forgejo provides](README.md).

Sensitive security-related issues should be reported to [security@forgejo.org](mailto:security@forgejo.org) using [encryption](https://keyoxide.org/security@forgejo.org).

## For everyone involved

- [Documentation](https://forgejo.org/docs/next/)
- [Code of Conduct](https://forgejo.org/docs/next/developer/COC/)
- [Bugs, features, security and others discussions](https://forgejo.org/docs/next/developer/DISCUSSIONS/)
- [Governance](https://forgejo.org/docs/next/developer/GOVERNANCE/)
- [Sustainability and funding](https://codeberg.org/forgejo/sustainability/src/branch/master/README.md)

## For contributors

- [Developer Certificate of Origin (DCO)](https://forgejo.org/docs/next/developer/DCO/)
- [Development workflow](https://forgejo.org/docs/next/developer/WORKFLOW/)

## For maintainers

- [Release management](https://forgejo.org/docs/next/developer/RELEASE/)
- [Secrets](https://forgejo.org/docs/next/developer/SECRETS/)
